#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import date
from configparser import ConfigParser
from os import path

ARCHIVO_NSIS = "installer.cfg"
ARCHIVO_NSIS_TEMPLATE = "installer_template.cfg"
ARCHIVO_CONF = "metadata.cfg"
SECCION = "metadata"
FECHA = "date"
VERSION = "version"

# Lee la informacion
metadata = ConfigParser()
metadata.read(ARCHIVO_CONF)
# Actualiza el numero de construccion
version = metadata[SECCION][VERSION].split('.')
version[-1] = str(int(version[-1]) + 1)
version = '.'.join(version)
# Actualisa la fecha y version
metadata[SECCION][VERSION] = version
metadata[SECCION][FECHA] = date.today().isoformat()
# Escribe el archivo resultante
with open(path.join(ARCHIVO_CONF), 'w') as archivo:
    metadata.write(archivo)

#Actualisar "installer.cfg" para pyNSIS
with open(path.join(ARCHIVO_NSIS_TEMPLATE), 'r') as archivo:
    template = archivo.read()
installer = template.format(version=version)
with open(path.join(ARCHIVO_NSIS), 'w') as archivo:
    archivo.write(installer)
