#!/bin/bash
# Script para generar los paquetes y subirlos a la intenet

source venv/bin/activate

# Actualisa la version y fecha
python3 updateBuild.py
cp metadata.cfg scet

# Borra las distribuciones viejas
rm -r dist
# Crea y sube la nueva version al servidor
python3 setup.py sdist bdist_wheel
twine upload dist/*

# Crear instalador windows
pip3 install --upgrade scet
pynsist installer.cfg

deactivate
